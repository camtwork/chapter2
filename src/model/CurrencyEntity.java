package model;

public class CurrencyEntity{
    private Double rate;
    private String date;
    public CurrencyEntity(double rate, String date){
        this.rate = rate;
        this.date = date;
    }
    public String getTimeStamp() {
        return date;
    }
    public Double getRate() {
        return rate;
    }
    public String toString() {
        return String.format("%s %.4f",date,rate);
    }
}