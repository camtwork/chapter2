package controller;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import model.Currency;
import view.CurrencyParentPane;
import view.TopPane;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Launcher extends Application {
    private static Stage primaryStage;
    private static Scene mainScene;
    private static FlowPane mainPane;
    private static TopPane topPane;
    private static CurrencyParentPane currencyParentPane;
    private static ArrayList<Currency> currencyList;
    private static ArrayList<String> validCode;
    private static Currency currentCurrency;

    public static void setCurrencyList(ArrayList<Currency> currencyList) {
        Launcher.currencyList = currencyList;
    }

    public static ArrayList<Currency> getCurrencyList() {
        return currencyList;
    }

    public static Currency getCurrentCurrency() {
        return currentCurrency;
    }

    public static void setCurrentCurrency(Currency currentCurrency) {
        Launcher.currentCurrency = currentCurrency;
    }

    public static ArrayList<String> getValidCode() {
        return validCode;
    }

    public static void setValidCode(ArrayList<String> validCode) {
        Launcher.validCode = validCode;
    }

    public void start(Stage arg) throws Exception {
        primaryStage = arg;
        primaryStage.setTitle("Currency Watcher");
        primaryStage.setResizable(false);
        Initialize.initialize_app();
        initMainPane();
        mainScene = new Scene(mainPane);
        this.primaryStage.setScene(mainScene);
        primaryStage.show();
        RefreshTask r = new RefreshTask();
        Thread th = new Thread(r);
        th.setDaemon(true);
        th.start();
    }
    public void initMainPane() {
        mainPane = new FlowPane();
        topPane = new TopPane();
        try {
            currencyParentPane = new CurrencyParentPane(currencyList);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mainPane.getChildren().add(topPane);
        mainPane.getChildren().add(currencyParentPane);
    }

    public static void refreshPane() {
        topPane.refreshPane();
        try {
            currencyParentPane.refreshPane(currencyList,currentCurrency);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}