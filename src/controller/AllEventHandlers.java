package controller;

import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import model.Currency;
import model.CurrencyEntity;

import java.util.ArrayList;
import java.util.Optional;

public class AllEventHandlers {
    public static void onRefresh(){
        try{
            Launcher.refreshPane();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void onAdd() {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Add Currency");
            dialog.setContentText("Currency code:");
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            Optional<String> code = dialog.showAndWait();
            if (code.isPresent()){
                if(Launcher.getValidCode().contains(code.get().toUpperCase())){
                    ArrayList<Currency> currency_list = Launcher.getCurrencyList();
                    Currency c = new Currency(code.get().toUpperCase());
                    ArrayList<CurrencyEntity> c_list = FetchData.fetch_range(c.getShortCode(),14);
                    c.setHistorical(c_list);
                    c.setCurrent(c_list.get(c_list.size()-1));
                    currency_list.add(c);
                    Launcher.setCurrencyList(currency_list);
                    Launcher.setCurrentCurrency(c);
                    Launcher.refreshPane();

                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle(null);
                    alert.setHeaderText(null);
                    alert.setContentText("Invalid Code. Please try again...");
                    alert.showAndWait();
                    AllEventHandlers.onAdd();
                }
            }
    }

    public static void onWatch(String code){

        ArrayList<Currency> currencyArrayList = Launcher.getCurrencyList();
        int index = -1;
        for(int i = 0 ; i < currencyArrayList.size() ; i++){
            if(currencyArrayList.get(i).getShortCode().equals(code)){
                index = i;
                break;
            }
        }
        if(index != -1){
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Add Watch");
            dialog.setContentText("Rate: ");
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            Optional<String> retrievedRate = dialog.showAndWait();
            if(retrievedRate.isPresent()){
                double rate = Double.parseDouble(retrievedRate.get());
                currencyArrayList.get(index).setWatch(true);
                currencyArrayList.get(index).setWatchRate(rate);
                Launcher.setCurrencyList(currencyArrayList);
                Launcher.refreshPane();
            }
        }
    }

    public static void onUnwatch(String code){
        ArrayList<Currency> currencyArrayList = Launcher.getCurrencyList();
        int index = -1;
        for(int i = 0; i < currencyArrayList.size(); i++){
            if(currencyArrayList.get(i).getShortCode().equals(code)){
                index = i;
                break;
            }
        }
        if(index != -1){
            currencyArrayList.get(index).setWatch(false);
            currencyArrayList.get(index).setWatchRate(0.0);
            Launcher.setCurrencyList(currencyArrayList);
            Launcher.refreshPane();
        }
    }
    public static void onDelete(String code){
        ArrayList<Currency> currency_list = Launcher.getCurrencyList();
        int index = -1;
        for(int i=0; i < currency_list.size(); i++){
            if(currency_list.get(i).getShortCode().equals(code)){
                index = i;
                break;
            }
        }
        if(index != -1){
            currency_list.remove(index);
            Launcher.setCurrencyList(currency_list);
            Launcher.setCurrentCurrency(null);
            Launcher.refreshPane();
        }
    }
}
