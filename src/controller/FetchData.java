package controller;

import model.CurrencyEntity;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

public class FetchData{
    private static final String apiKey = "f86ba4364e8898d589f5";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static ArrayList<String> getValidCode(){

        ArrayList<String> validCode = new ArrayList<>();

        String url = String.format(new String("https://free.currconv.com/api/v7/currencies?apiKey=%s"), apiKey);
        try {
            String retrievedJson = IOUtils.toString(new URL(url), Charset.defaultCharset());
            JSONObject obj = new JSONObject(retrievedJson).getJSONObject("results");
            Iterator keysToCopyIterator = obj.keys();
            while(keysToCopyIterator.hasNext()){
                String key = (String) keysToCopyIterator.next();
                validCode.add(key);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return validCode;
    }
    public static ArrayList<CurrencyEntity> fetch_range(String src,int N){
        N--;
        String dateEnd = LocalDate.now().format(formatter);
        ArrayList<String> dateStart = new ArrayList<>();
        for(int i = 1; i <= (int) Math.ceil((float)N/8); i++)
        {
            if(i == (int) Math.ceil((float)N/8)){
                dateStart.add(LocalDate.now().minusDays(N-((i-1)*8) + (i-1)*8).format(formatter));
                break;
            }
            dateStart.add(LocalDate.now().minusDays(i*8).format(formatter));
        }
        ArrayList<String> url = new ArrayList<>();
        for(int i = 0; i < (int) Math.ceil((float)N/8); i++){
            if(i == 0)
                url.add(String.format("https://free.currconv.com/api/v7/convert?q=%s_THB&compact=ultra&date=%s&endDate=%s&apiKey=%s",src,dateStart.get(i),dateEnd,apiKey));
            else
                url.add(String.format("https://free.currconv.com/api/v7/convert?q=%s_THB&compact=ultra&date=%s&endDate=%s&apiKey=%s",src,dateStart.get(i),dateStart.get(i-1),apiKey));
        }
        ArrayList<CurrencyEntity> histList = new ArrayList<>();

        ArrayList<String> retrievedJson = new ArrayList<>();
        try{
            for(int i = 0; i < (int) Math.ceil((float)N/8); i++)
            {
                retrievedJson.add(IOUtils.toString(new URL(url.get(i)), Charset.defaultCharset()));
            }
        } catch(MalformedURLException e){
            System.out.println("Encountered a Malformed Url exception");
        } catch(IOException e){
            System.out.println("Encounter an IO exception");
        }

        ArrayList<JSONObject> obj = new ArrayList<>();
        for(String s : retrievedJson){
            obj.add(new JSONObject(s).getJSONObject(String.format("%s_THB", src)));
        }
        for(JSONObject ob : obj){
            Iterator keysToCopyIterator = ob.keys();
            while(keysToCopyIterator.hasNext()){
                String key = (String) keysToCopyIterator.next();
                Double rate = Double.parseDouble(ob.get(key).toString());
                histList.add(new CurrencyEntity(rate,key));
            }

            histList.sort(new Comparator<CurrencyEntity>(){
                public int compare(CurrencyEntity a, CurrencyEntity b){
                    return a.getTimeStamp().compareTo(b.getTimeStamp());
                }
            });
        }
        return histList;
    }
}