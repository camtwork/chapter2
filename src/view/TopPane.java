package view;

import controller.AllEventHandlers;
import controller.Launcher;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.util.StringConverter;
import model.Currency;

import java.time.LocalDateTime;

public class TopPane extends FlowPane {
    private Button refresh;
    private Button add;
    private ComboBox<Currency> options = new ComboBox<>();
    private Label update;
    public TopPane(){
        this.setPadding(new Insets(10));
        this.setHgap(10);
        this.setPrefSize(640,20);
        this.add = new Button("Add");
        this.refresh = new Button("Refresh");
        refresh.setOnAction((EventHandler<ActionEvent>) event -> AllEventHandlers.onRefresh());
        add.setOnAction((EventHandler<ActionEvent>) event -> AllEventHandlers.onAdd());
        update = new Label();
        this.getChildren().addAll(refresh,add,update);
        this.refreshPane();
    }

    public void refreshPane() {
        update.setText(String.format("Last update: %s", LocalDateTime.now().toString()));
        updateOptions();
    }

    public void updateOptions(){
        if(Launcher.getCurrencyList().size() == 0){
            this.options.getItems().clear();
            return;
        }
        if(Launcher.getCurrencyList().size() != options.getItems().size()){
            if(options.getItems().size() != 0)
                this.options.getItems().clear();
            this.getChildren().remove(options);
            this.options.setConverter(new StringConverter<Currency>() {
                @Override
                public String toString(Currency currency) {
                    if(currency == null)
                        return "";
                    return currency.getShortCode();
                }

                @Override
                public Currency fromString(String s) {
                    return null;
                }
            });
            this.options.setItems(FXCollections.observableArrayList(Launcher.getCurrencyList()));
            this.options.setValue(Launcher.getCurrencyList().get(Launcher.getCurrencyList().size()-1));
            this.options.getSelectionModel().selectedItemProperty().addListener((ops,oldval,newval) -> {
                if(newval != null){
                    Launcher.setCurrentCurrency(newval);
                    Launcher.refreshPane();
                }
            });
            this.getChildren().add(options);
        }
    }
}