package view;

import javafx.geometry.Insets;
import javafx.scene.layout.FlowPane;
import model.Currency;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class CurrencyParentPane extends FlowPane {
    public CurrencyParentPane(ArrayList<Currency> currencyArrayList) throws ExecutionException, InterruptedException {
        this.setPadding(new Insets(0));
        refreshPane(currencyArrayList, currencyArrayList.get(0));
    }
    public void refreshPane(ArrayList<Currency> currencyList, Currency showCurrency) throws ExecutionException, InterruptedException {
        this.getChildren().clear();
        this.getChildren().add(new CurrencyPane(showCurrency));
        for(int i=0 ; i<currencyList.size() ; i++) {
            if(currencyList.get(i) != showCurrency){
                CurrencyPane cp = new CurrencyPane(currencyList.get(i));
                this.getChildren().add(cp);
            }
        }
    }
}