package view;

import controller.AllEventHandlers;
import controller.draw.DrawGraphTask;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.Currency;
import view.subpane.CurrencyInfoPane;
import view.subpane.CurrencyTopPane;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class CurrencyPane extends BorderPane {
    private Currency currency;
    private Button watch;
    private Button unwatch;
    private Button delete;
    public CurrencyPane(Currency currency){
        if(currency == null)
            return;
        this.watch = new Button("Watch");
        this.unwatch = new Button("Unwatch");
        this.delete = new Button("Delete");
        this.watch.setOnAction((EventHandler<ActionEvent>) event -> AllEventHandlers.onWatch(currency.getShortCode()));
        this.unwatch.setOnAction((EventHandler<ActionEvent>) event -> AllEventHandlers.onUnwatch(currency.getShortCode()));
        this.delete.setOnAction((EventHandler<ActionEvent>) event -> AllEventHandlers.onDelete(currency.getShortCode()));
        this.setPadding(new Insets(0));
        this.setPrefSize(640,300);
        this.setStyle("-fx-border-color: black");
        try{
            this.refreshPane(currency);
        }catch(ExecutionException e){
            System.out.println("Encountered an execution exception");
        }catch(InterruptedException e){
            System.out.println("Encountered an interrupted exception");
        }

    }

    public void refreshPane(Currency currency) throws ExecutionException, InterruptedException{
        this.currency = currency;
        FutureTask currencyTopTask = new FutureTask<HBox>(new CurrencyTopPane(watch,unwatch,delete));
        FutureTask currencyInfoTask = new FutureTask<Pane>(new CurrencyInfoPane(currency));
        FutureTask graphTask = new FutureTask<VBox>(new DrawGraphTask(currency));
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(currencyTopTask);
        executor.execute(currencyInfoTask);
        executor.execute(graphTask);
        HBox currencyTopPane = (HBox) currencyTopTask.get();
        Pane currencyInfoPane = (Pane) currencyInfoTask.get();
        VBox currencyGraph = (VBox) graphTask.get();
        this.setTop(currencyTopPane);
        this.setLeft(currencyInfoPane);
        this.setCenter(currencyGraph);
    }
}